FROM node:20-alpine
RUN apk add --no-cache git
RUN git clone https://github.com/Exp1o1/Physics.git
WORKDIR /Physics
RUN npm install
CMD npm start